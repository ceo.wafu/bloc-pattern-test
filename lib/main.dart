import 'package:flutter/material.dart';
import 'bloc/employeeBloc.dart';
import 'model/employee.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocPattern(),
    );
  }
}

class BlocPattern extends StatefulWidget {
  @override
  _BlocPatternState createState() => _BlocPatternState();
}

class _BlocPatternState extends State<BlocPattern> {
  final EmployeeBloc employeeBloc = EmployeeBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    employeeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Bloc Pattern"),
        ),
        body: Container(
            child: StreamBuilder<List<Employee>>(
                stream: employeeBloc.employeeListStream,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Employee>> snapshot) {        
                  if (snapshot.hasError)
                    return Text('Error: ${snapshot.error}');
                  else if (!snapshot.hasData)
                    return Text("Loading...");
                  else
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 5.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(20.0),
                                child: Text(
                                  "${snapshot.data[index].id}.",
                                  style: TextStyle(fontSize: 20.0),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "${snapshot.data[index].name}.",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    Text(
                                      "${snapshot.data[index].salary}",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                child: IconButton(
                                  icon: Icon(Icons.thumb_up),
                                  color: Colors.green,
                                  onPressed: () {
                                    employeeBloc.employeeSalaryIncrement
                                        .add(snapshot.data[index]);
                                  },
                                ),
                              ),
                              Container(
                                child: IconButton(
                                  icon: Icon(Icons.thumb_down),
                                  color: Colors.red,
                                  onPressed: () {
                                    employeeBloc.employeeSalaryDecrement
                                        .add(snapshot.data[index]);
                                  },
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    );
                })));
  }
}
