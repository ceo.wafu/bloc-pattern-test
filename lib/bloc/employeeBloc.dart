//imports
//List of Employees
//Stream COntrollers
//Stream Sink Getters
//Constructor- Add Data, Listen to CHanges
//Core Functions
//Dispose

//imports
import 'dart:async';
import '../model/employee.dart';
import 'package:rxdart/rxdart.dart';

class EmployeeBloc {
//List of Employees
  List<Employee> employeeList = [
    Employee(id: 1, name: "Manikanta", salary: 5342),
    Employee(id: 2, name: "Manikanta2", salary: 25342),
    Employee(id: 3, name: "Manikanta3", salary: 35342),
    Employee(id: 4, name: "Manikanta4", salary: 45342),
    Employee(id: 5, name: "Manikanta5", salary: 53542)
  ];

//Stream Controllers
  final employeeListStreamController = BehaviorSubject<List<Employee>>();
  //for inc and dec
  final employeeListSalIncController = BehaviorSubject<Employee>();
  final employeeListSalDecController = BehaviorSubject<Employee>();

//Stream Sink Getters
  Stream<List<Employee>> get employeeListStream =>
      employeeListStreamController.stream;
  StreamSink<List<Employee>> get employeeListStreamSink =>
      employeeListStreamController.sink;
  StreamSink<Employee> get employeeSalaryIncrement =>
      employeeListSalIncController.sink;
  StreamSink<Employee> get employeeSalaryDecrement =>
      employeeListSalDecController.sink;

//Constructor- Add Data, Listen to CHanges
  EmployeeBloc() {
    employeeListStreamController.add(employeeList);
    employeeListSalIncController.stream.listen(_incrementSalary);
    employeeListSalDecController.stream.listen(_decrementSalary);
  }

//Core Functions
  _incrementSalary(Employee employee) {
    print("called increment on " + employee.name);
    employeeList[employee.id - 1].salary =
        (employee.salary * 120 / 100).floor();
  }

  _decrementSalary(Employee employee) {
    print("called decrement on " + employee.name);
    employeeList[employee.id - 1].salary = (employee.salary * 80 / 100).floor();
  }

//Dispose
  void dispose() {
    employeeListSalDecController.close();
    employeeListSalIncController.close();
    employeeListStreamController.close();
  }
}
