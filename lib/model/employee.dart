import 'dart:convert';

Employee employeeFromJson(String str) => Employee.fromJson(json.decode(str));

String employeeToJson(Employee data) => json.encode(data.toJson());

List<Employee> employeeListFromJson(String str) => new List<Employee>.from(json.decode(str).map((x) => Employee.fromJson(x)));

String employeeListToJson(List<Employee> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));


class Employee {
    int id;
    String name;
    int salary;

    Employee({
        this.id,
        this.name,
        this.salary,
    });

    factory Employee.fromJson(Map<String, dynamic> json) => new Employee(
        id: json["id"],
        name: json["name"],
        salary: json["salary"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "salary": salary,
    };
}
